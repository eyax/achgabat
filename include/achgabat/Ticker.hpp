#ifndef AG_TICKER_HPP
#define AG_TICKER_HPP

#include <SFML/Graphics.hpp>

/**
 * @file Ticker.hpp
 * @author Underdisk
 */

namespace ag {

class Ticker
{
    public:
        Ticker();
        Ticker(const float& tickrate);
        virtual ~Ticker();

        void setTickrate(const float& tickrate) {this->tickrate = tickrate;}
        bool check(const sf::Time& dt);

    protected:

        float tickrate;

    private:

        float m_tick;
};

} // namespace ag

#endif // AG_TICKER_HPP
