#ifndef AG_CSVPARSER_HPP
#define AG_CSVPARSER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

namespace ag {

class CsvParser
{
    public:
        CsvParser(std::string fileName, char delimiter);
        virtual ~CsvParser();

        std::string getLine(unsigned int line);
        std::string getValue(unsigned int line, unsigned int column);

    protected:

    private:

        char m_delimiter;
        std::vector<std::string> m_lines;
};

} // namespace ag

#endif // AG_CSVPARSER_HPP
