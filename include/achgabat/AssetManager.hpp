#ifndef AG_ASSETMANAGER_HPP
#define AG_ASSETMANAGER_HPP

#include <SFML/Graphics.hpp>
#include <string>

namespace ag {

class AssetManager
{
	public:
		static AssetManager instance;

    public:
        virtual ~AssetManager()
        {   }

        //

        void loadTexture(int index, const std::string& fileName)
        {
            sf::Texture tex;
            if(tex.loadFromFile(fileName))
            {
                m_textures[index] = tex;
            }
        }

        sf::Texture& getTexture(int index)
        {
            return m_textures.at(index);
        }

        void loadFont(int index, const std::string& fileName)
        {
            sf::Font font;
            if(font.loadFromFile(fileName))
            {
                m_fonts[index] = font;
            }
        }

        sf::Font& getFont(int index)
        {
            return m_fonts.at(index);
        }

        void loadImage(int index, const std::string& fileName)
        {
            sf::Image image;
            if(image.loadFromFile(fileName))
            {
                m_images[index] = image;
            }
        }

        sf::Image& getImage(int index)
        {
            return m_images.at(index);
        }

        /** ABOUT DYNAMIC ALLOCATION

            We are now entering a pretty repetitive part of the code
            This part is for every dynamic allocation stuff, including

            ('Element' can be 'Image', 'Texture', 'Font'...)

            void dynloadElement(index, fileName) : Dynamically loading the element
            void dyndelElement(index) : Dynamically deleting the element from memory
            void dyndelElements() : Dynamically deleting all elements
            Element* dyngetElement(index) : Returns pointer to element
            void dyndelAll() : Dynamically deleting every dynamically loaded assets from the asset manager instance.
                [Call dyndelAll() at the end of your program to avoid leaks.]

        **/

        //Image

        void dynloadImage(int index, const std::string& fileName)
        {
            sf::Image* image = new sf::Image;
            if(image->loadFromFile(fileName))
            {
                m_imagesPtr[index] = image;
            }
        }

        sf::Image* dyngetImage(int index)
        {
            return m_imagesPtr.at(index);
        }

        void dyndelImage(int index)
        {
            delete m_imagesPtr.at(index);
            m_imagesPtr.erase(index);
        }

        void dyndelImages()
        {
            for(std::map<int, sf::Image*>::iterator itr = m_imagesPtr.begin(); itr != m_imagesPtr.end(); ++itr)
            {
                delete itr->second; /**Deleting the second element of the std::map (sf::Image*)**/
            }
            m_imagesPtr.clear();
        }

        //Font

        void dynloadFont(int index, const std::string& fileName)
        {
            sf::Font* font = new sf::Font;
            if(font->loadFromFile(fileName))
            {
                m_fontsPtr[index] = font;
            }
        }

        sf::Font* dyngetFont(int index)
        {
            return m_fontsPtr.at(index);
        }

        void dyndelFont(int index)
        {
            delete m_fontsPtr.at(index);
            m_fontsPtr.erase(index);
        }

        void dyndelFonts()
        {
            for(std::map<int, sf::Font*>::iterator itr = m_fontsPtr.begin(); itr != m_fontsPtr.end(); ++itr)
            {
                delete itr->second; /**Deleting the second element of the std::map (sf::Font*)**/
            }
            m_fontsPtr.clear();
        }

        //Texture

        void dynloadTexture(int index, const std::string& fileName)
        {
            sf::Texture* texture = new sf::Texture;
            if(texture->loadFromFile(fileName))
            {
                m_texturesPtr[index] = texture;
            }
        }

        sf::Texture* dyngetTexture(int index)
        {
            return m_texturesPtr.at(index);
        }

        void dyndelTexture(int index)
        {
            delete m_texturesPtr.at(index);
            m_texturesPtr.erase(index);
        }

        void dyndelTextures()
        {
            for(std::map<int, sf::Texture*>::iterator itr = m_texturesPtr.begin(); itr != m_texturesPtr.end(); ++itr)
            {
                delete itr->second; /**Deleting the second element of the std::map (sf::Texture*)**/
            }
            m_texturesPtr.clear();
        }

        //All

        void dyndelAll()
        {
            dyndelImages();
            dyndelFonts();
            dyndelTextures();
        }

    protected:

    private:
        std::map<int, sf::Texture> m_textures;
        std::map<int, sf::Font> m_fonts;
        std::map<int, sf::Image> m_images;

        std::map<int, sf::Texture*> m_texturesPtr;
        std::map<int, sf::Font*> m_fontsPtr;
        std::map<int, sf::Image*> m_imagesPtr;

		AssetManager()
		{   }
};

} // namespace ag

#endif // AG_ASSETMANAGER_HPP
