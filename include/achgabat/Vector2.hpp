#ifndef AG_VECTOR2_HPP
#define AG_VECTOR2_HPP

#include <SFML/System/Vector2.hpp>
#include <cmath>

/**
 * @file Vector2.hpp
 * @author Underdisk
 */

namespace ag {

template<class T>
class Vector2 : public sf::Vector2<T>
{
    public:
        Vector2() {}
        Vector2(T x, T y) : sf::Vector2<T>(x, y) {}

		Vector2<T> operator=(const Vector2<T>& vector) { this->x = vector.x;  this->y = vector.y; return *this; }
		Vector2<T> operator=(const sf::Vector2<T>& vector) { this->x = vector.x;  this->y = vector.y; return *this; }

		Vector2<T> operator+(const T& number) { return Vector2<T>(this->x + number, this->y + number); }
		Vector2<T> operator-(const T& number) { return Vector2<T>(this->x - number, this->y - number); }
		Vector2<T> operator*(const T& number) { return Vector2<T>(this->x * number, this->y * number); }
        Vector2<T> operator/(const T& number) { return Vector2<T>(this->x / number, this->y / number); }

		Vector2<T> operator+=(const T& number) { this->x += number; this->y += number; return *this; }
		Vector2<T> operator-=(const T& number) { this->x -= number; this->y -= number; return *this; }
		Vector2<T> operator*=(const T& number) { this->x *= number; this->y *= number; return *this; }
		Vector2<T> operator/=(const T& number) { this->x /= number; this->y /= number; return *this; }

		Vector2<T> operator+(const Vector2<T>& vector) { return Vector2<T>(this->x + vector.x, this->y + vector.y); }
		Vector2<T> operator-(const Vector2<T>& vector) { return Vector2<T>(this->x - vector.x, this->y - vector.y); }
		Vector2<T> operator*(const Vector2<T>& vector) { return Vector2<T>(this->x * vector.x, this->y * vector.y); }
		Vector2<T> operator/(const Vector2<T>& vector) { return Vector2<T>(this->x / vector.x, this->y / vector.y); }

		Vector2<T> operator+=(const Vector2<T>& vector) { this->x += vector.x; this->y += vector.y; return *this; }
		Vector2<T> operator-=(const Vector2<T>& vector) { this->x -= vector.x; this->y -= vector.y; return *this; }
		Vector2<T> operator*=(const Vector2<T>& vector) { this->x *= vector.x; this->y *= vector.y; return *this; }
		Vector2<T> operator/=(const Vector2<T>& vector) { this->x /= vector.x; this->y /= vector.y; return *this; }

        float getLengthSqrt() { return this->x * this->x + this->y * this->y; }
		float getLength() { return std::sqrt(this->x * this->x + this->y * this->y); }

		void normalize()
		{
			float len = getLength();
			this->x /= len;
			this->y /= len;
		}

        Vector2<T> getNormalized()
        {
			float len = getLength();
            return Vector2<T>(this->x / len, this->y / len);
        }

		/*float dot(const Vector<T> &a, const Vector<T> &b) { return a.x * b.x + a.x + b.x; }
		float dot(const Vector<T> &vector) { return this->x * vector.x + this->x + vector.x; }*/

    protected:

    private:
};

typedef Vector2<float> Vector2f;
typedef Vector2<int> Vector2i;
typedef Vector2<unsigned int> Vector2u;

} // namespace ag

#endif // AG_VECTOR2_HPP
