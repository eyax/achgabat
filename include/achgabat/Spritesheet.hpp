#ifndef AG_SPRITESHEET_HPP
#define AG_SPRITESHEET_HPP

#include <achgabat/Animation.hpp>
#include <achgabat/Application.hpp>
#include <SFML/Graphics.hpp>
#include <map>

namespace ag {

class Spritesheet : public sf::Transformable, public sf::Drawable
{
    public:
        Spritesheet();
        virtual ~Spritesheet();

        void setTexture(const sf::Texture& texture);
        void addAnimation(ag::Animation animation, int index);
        void getCurrentFrame(const sf::Time& dt);
        void setAnimation(int index);
        void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        void pause();
        void resume();
        void setFrame(size_t frame);

        bool isPaused(){return m_animations[m_currentAnimation].isPaused();}

        void update(sf::Time& dt);

    protected:

    private:

        sf::Texture m_texture;
        sf::Sprite m_sprite;
        std::vector<sf::FloatRect> m_rects;
        std::map<int, ag::Animation> m_animations;
        int m_currentAnimation;


};

} // namespace ag

#endif // AG_SPRITESHEET_HPP
