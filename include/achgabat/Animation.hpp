#ifndef AG_ANIMATION_HPP
#define AG_ANIMATION_HPP

#include <achgabat/Ticker.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <vector>

namespace ag {

typedef enum Slice
{
    Horizontal,
    Vertical
} Slice;

class Animation
{

    public:
        Animation();
        virtual ~Animation();

        void addFrame(sf::IntRect frame);
        void setFramerate(float framerate);
        sf::IntRect& getCurrentFrame(sf::Time& dt);
        void slice(size_t x, size_t y, size_t width, size_t height, size_t numberOfFrames, ag::Slice sliceConfig = ag::Slice::Horizontal);

        void setFrame(size_t frame);
        void pause();
        void resume();

        bool isPaused(){return m_isPaused;}

    protected:

    private:

    ag::Ticker m_ticker;
    std::vector<sf::IntRect> m_rects;
    size_t m_currentFrame;
    bool m_isPaused;





};

} // namespace ag

#endif // AG_ANIMATION_HPP
