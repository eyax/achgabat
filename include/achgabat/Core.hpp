#ifndef CORE_HPP_INCLUDED
#define CORE_HPP_INCLUDED

#define ACHGABAT_VERSION_MAJOR 0
#define ACHGABAT_VERSION_MINOR 1
#define ACHGABAT_VERSION_PATCH 2

/**
 * @file Updatable.hpp
 * @author Underdisk
 */

namespace ag {

    enum
    {
        NoErrorStatus = 0
    };

}

#endif // CORE_HPP_INCLUDED
