#include "achgabat/Spritesheet.hpp"

namespace ag {

Spritesheet::Spritesheet() : m_currentAnimation(0)
{
    m_sprite.setTexture(m_texture);
}

Spritesheet::~Spritesheet()
{
    //dtor
}

void Spritesheet::setTexture(const sf::Texture& texture)
{
    m_texture = texture;
}

void Spritesheet::addAnimation(ag::Animation animation, int index)
{
    m_animations[index] = animation;
}

void Spritesheet::setAnimation(int index)
{
    m_currentAnimation = index;
}

void Spritesheet::update(sf::Time& dt)
{
    m_sprite.setTextureRect(m_animations.at(m_currentAnimation).getCurrentFrame(dt));
}

void Spritesheet::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(m_sprite, getTransform());
}


void Spritesheet::pause()
{
    m_animations[m_currentAnimation].pause();
}

void Spritesheet::resume()
{
    m_animations[m_currentAnimation].resume();
}

} // namespace ag
