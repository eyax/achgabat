#include "achgabat/Animation.hpp"

namespace ag {

Animation::Animation() : m_currentFrame(0), m_isPaused(false)
{
    //ctor
}

Animation::~Animation()
{
    //dtor
}

void Animation::addFrame(sf::IntRect frame)
{
    m_rects.push_back(frame);
}

sf::IntRect& Animation::getCurrentFrame(sf::Time& dt)
{
    if(m_ticker.check(dt) && !m_isPaused)
    {
        ++m_currentFrame;
        if(m_currentFrame >= m_rects.size()) m_currentFrame = 0;
    }

    return m_rects[m_currentFrame];
}

void Animation::setFramerate(float framerate)
{
    m_ticker.setTickrate(framerate);
}

void Animation::slice(size_t x, size_t y, size_t width, size_t height, size_t numberOfFrames, ag::Slice sliceConfig)
{
    m_rects.clear();
      sf::IntRect rect;
        rect.top = y;
        rect.left = x;
        rect.width = width;
        rect.height = height;

        m_rects.push_back(rect);

    if(sliceConfig == ag::Slice::Horizontal)
    {
        for(size_t i = 1; i < numberOfFrames; ++i)
        {
            rect.left = x+i*width;
            m_rects.push_back(rect);
        }
    }
    else
    {
        for(size_t i = 1; i < numberOfFrames; ++i)
        {
            rect.left = y+i*height;
            m_rects.push_back(rect);
        }
    }
}

void Animation::setFrame(size_t frame)
{
    m_currentFrame = frame;
}

void Animation::pause()
{
    m_isPaused = true;
}

void Animation::resume()
{
    m_isPaused = false;
}


} // namespace ag
