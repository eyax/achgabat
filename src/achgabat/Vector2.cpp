#include "achgabat/Vector2.hpp"

/**
 * @file Vector2.cpp
 * @author Underdisk
 */

/**
 * This file is completely empty. ag::Vector2 is a template class and so everything has to be made in one file.
 * For definitions, please relate to the header file Vector2.hpp
 */

namespace ag {


} // namespace ag
