#include "achgabat/CsvParser.hpp"

namespace ag {

CsvParser::CsvParser(std::string fileName, char delimiter)
{
    std::ifstream file(fileName);
    std::string line;
    m_delimiter = delimiter;
    while(std::getline(file, line))
    {
        m_lines.push_back(line);
    }

    file.close();
}

CsvParser::~CsvParser()
{

}

std::string CsvParser::getLine(unsigned int line)
{
    return m_lines[line];
}

std::string CsvParser::getValue(unsigned int line, unsigned int column)
{
    std::string str;
    std::istringstream iss(m_lines[line]);
    for(unsigned int i = 0; i <= column; ++i)
    {
        str.clear();
        getline(iss, str, m_delimiter);
    }

    return str;
}



} // namespace ag
