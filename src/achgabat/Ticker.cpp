#include "achgabat/Ticker.hpp"

/**
 * @file Ticker.cpp
 * @author Underdisk
 */

namespace ag {

Ticker::Ticker()
    : m_tick(0.0f)
{
    tickrate = 1.0f;
}

Ticker::Ticker(const float& tickrate)
    : m_tick(0.0f)
{
    this->tickrate = 1.0f/tickrate;
}

bool Ticker::check(const sf::Time& dt)
{
    m_tick += dt.asSeconds();
    if(m_tick >= tickrate)
    {
        m_tick = 0.0f;
        return true;
    }

    return false;
}

Ticker::~Ticker()
{
    //dtor
}

} // namespace ag
